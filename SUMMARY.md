# Summary

* [Introduction](README.md)
* [Chapter 0 Introduction](Chapter0.md)
    * [02 Math Notion and Term](Chapter02.md)
    * [03 Definitions, Theorems, and Proofs](Chapter03.md)
* [Chapter 1 Regular languages](Chapter1.md)
    * [1.1 Finite Automata](Chapter11.md)
    * [1.2 Nondeterministic Finite Automaton](Chapter12.md)      
    * [1.3 Regular Expressions](Chapter13.md)
    * [Practice](Practice1.md)
        * [Design Finite Automaton](Design Finite Automaton.md)
        * [Regular expression GNFA DFA](Regular expression GNFA DFA.md)
            * [NFA to DFA](NFA to DFA.md)
            * [DFA to Regular Expression](DFA to Regular Expression.md)
            * [Regular expression to NFA](Regular expression to NFA.md)      
        * [Minimization of DFA](Minimization of DFA.md)
        * [Pumping Lemma](Pumping Lemma.md) 
* [Chapter 2 Context-Free Languages](Chapter2.md)
    * [2.1 Context-Free Grammars](Chapter21.md)
    * [2.2 Pushdown Automata](Chapter22.md)
    * [2.3 Non-Context-Free Languages](Chapter23.md)
    * [Practice](Practice2.md)
        * [Convert finite state automaton to context-free grammar](Convert finite state automaton to context-free grammar.md)
        * [Convert regular expression to context-free grammar](convertregularexpressiontocontextfreegrammar.md)
        * [Design context free grammar](Design context free automaton.md)
        * [CFG to Chomsky Normal Form](CFGtoChomskyNormalForm.md)
        * [Design Pushdown Automaton](design pushdown automaton.md)
        * [Context Free Grammar to Pushdown Automata](context free grammar to pushdown automata.md)                      
* [Exam](Exam.md)
    * [Exam 1 Review](Exam1Review.md)
    * [Exam 2 Review](Exam2Review.md)
    * [Exam 3 Review](Exam3Review.md)
    * [Final Review](FinalReview.md)
    * [Spring 2017 Exam 1](Spring2017Exam1.md)
    * [Fall 2017 Exam 1](Fall2017Exam1.md)
    * [Spring 2018 Exam 1](Spring 2018 Exam 1.md)
    * [Summer 2018 Exam 1](Summer2018Exam1.md)
    * [Spring 2020 Exam 1](Spring2020Exam1.md)  
    * [Fall 2020 Exam 1](Fall2020Exam1.md)    
    * [Spring 2017 Exam 2](Spring2017Exam2.md)
    * [Spring 2017 Exam 2 001](Spring2017Exam2001.md)
    * [Spring 2018 Exam 2](Spring2018Exam2.md)      
    * [Fall 2020 Exam 2](Fall2020Exam2.md)        
* [Portland State University](Portland State University.md)      
    * [Study Note](Portland State University Study Note.md)
    * [Lecture Note](Portland State University Lecture Note.md)      
* [Stony Brook University](Stony Brook University.md)      
    * [Study Note](Stony Brook University Study Note.md)
    * [Lecture Note](Stony Brook University Lecture Note.md)      
                
          

      











    

