1. Check unreachable state
2. Define DFA
3. DFA to GNFA to regular expression
4. GNFA to DFA
5. Regular expression to GNFA
6. Induction to prove that the power set size
7. State minimization
8. Intersection and Union
9. Pumping lemma
    

<iframe
height="842"
width="100%" 
src="https://docs.google.com/document/d/e/2PACX-1vRPiKY41ZGv8rGMXwwc8uq660hJRP2a9t7TR2iBy5K7r0ZF1fLB57ePBKBXHErhnwgkAmVW7aGw_cfI/pub?embedded=true"></iframe>
