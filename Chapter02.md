# Math Notion and Term      

## Sets        

A set is a group of objects. 

The order does not matter in a set.        

```python
# {7, 21, 57} is the same as {57, 7, 21}
# True
print({7, 21, 57} == {57, 7, 21})
```

The repetition does not matter in a sequence.      

```python
# {7, 7, 21, 57} is the same as {7, 21, 57}
# True
print({7, 7, 21, 57} == {7, 21, 57})
```

           

1. elements        
    - the objects in a set        
2. infinite set
    - natural number {1, 2, 3,...}
    - integers {..., -2, -1, 0, 1, 2,...}        
3. empty set        
4. singleton set
5. union
6. Venn diagram

## Sequences and tuples        


1.A sequence of objects is a list of these objects in some order. 

The order does matter in a sequence.        

```python
# (7, 21, 57) is not the same as (57, 7, 21)
# False
print((7, 21, 57) == (57, 7, 21))
```



The repetition does matter in a sequence.            

```python
# (7, 7, 21, 57) is not the same as (7, 21, 57)
# False
print((7, 7, 21, 57) == (7, 21, 57))
```


2.A tuple is the finite sequence.      

3.Sequences and tuples      

- infinite sequence        
- finite sequence is called tuple
    + A sequence with k elements is a k-tuple        
    + A 2-tuple is called an ordered pair      


## Power set      

## cross product        





## Functions and relations      

1. `ƒ(a) = b`
    - A function is an object that sets up an input-output relationship.        
        + The `ƒ` is the function, `a` is the input, and `b` is the output.        
    - A function is called a mapping.        
        + The `ƒ` maps `a` to `b`.        

2. domain and range
    - domain is input
    - range is output        
    - `ƒ: D -> R`        

3. the infix notation and prefix notation
    - `a + b`
    - `+ a b`

4. the predicate and property     
    - TRUE
    - FALSE

## Graphs        

1. nodes and vertices        
2. edges
    - undirected graph        
        + the edge (i, j) is the same as edge (j, i)
    - directed graph        
        + outdegree
        + indegree        
3. degree is the edges at a particular node        
4. labeled graph        
5. subgraph                






## Strings and languages        

1. alphabet      
2. symbols      
3. length `|w|`      
4. empty string `|ε|`
5. reverse      
6. substring        
7. concatenation       







## Boolean logic        

1. not `¬`
2. and `∧`
3. or `∨`








 
        

        


## Lecture note
<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1XXc8pzKaGjGnvFcRuNQyWQEbvG4nVAYE/preview"></iframe>

## Practice
<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1bs5vT6-DdS3RljJswS47TXZ7n7CC2CFj/preview"></iframe>



































