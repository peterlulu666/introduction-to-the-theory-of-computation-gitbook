# Stony Brook University Lecture Note      

## Finite Automaton      

### Finite Automata: Informal     

<iframe 
width="100%" height="480"
src="https://drive.google.com/file/d/1wgQZ8eAVbi--D7ELPy71nib7LoFvlRqI/preview"></iframe>      

### Formal Definition of a Finite Automaton      

<iframe 
width="100%" height="480"
src="https://drive.google.com/file/d/1XppVWU_pWWVnZvoOeLjfyRfFK_nTCxuD/preview"></iframe> 


### Formal Definition of Computation      
<iframe src="https://drive.google.com/file/d/1wAiTNrG3gXDnlKiQmuYKr8rOy5Drrh2O/preview" width="640" height="480"></iframe>

### The Regular Operations      
<iframe src="https://drive.google.com/file/d/199YJkX9Aei8odbQm45ew6A3zq5XLobAz/preview" width="640" height="480"></iframe>

### Nondeterminism      
<iframe src="https://drive.google.com/file/d/1kJWzQ3yZEkV35ATnC8l6OeGhqFvfo8sd/preview" width="640" height="480"></iframe>      

### Formal Definition of a Nondeterministic Finite Automaton      
<iframe src="https://drive.google.com/file/d/18m5znztyzf5LbDFMFDym7I9NJ0IRJK2S/preview" width="640" height="480"></iframe>      

### Closure under the Regular Operations      
<iframe src="https://drive.google.com/file/d/1A-wS81PFc_PtqYv2WYOu9WhfatvYLYw1/preview" width="640" height="480"></iframe>      

## Context-Free Grammars

### Context-Free Grammars and Languages       
<iframe src="https://drive.google.com/file/d/15nRv9xPPAoZ_aGOjSnzOVLPfDbKaiNGf/preview" width="640" height="480"></iframe>

### Properties of Context-Free Grammars        
<iframe src="https://drive.google.com/file/d/1-kWgg56S_tUDI_cvlJk7evzOpB7RDfJG/preview" width="640" height="480"></iframe>

### Chomsky and Greibach Normal Forms        
<iframe src="https://drive.google.com/file/d/1T6vwMSN3-SfEnjQ8GLpry_MNycRxzFnq/preview" width="640" height="480"></iframe>















