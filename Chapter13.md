# Regular Expressions        

## Regular expression GNFA DFA      

- GNFA → DFA        
- DFA → GNFA → Regular expression        
- Regular expression → GNFA               
- Regular expression ⇄ GNFA ⇄ DFA        


## Regular expression      

Say that R is a regular expression if R is      

- a for some a in the alphabet Σ      
- ε      
- ∅      
- (R1 ∪ R2), where R1 and R2 are regular expressions            
- (R1 ◦ R2), where R1 and R2 are regular expressions      
- (R1 *), where R1 is a regular expression        

## Regular expression to GNFA      

<iframe      
height="842"
width="100%"      
src="https://docs.google.com/document/d/e/2PACX-1vTanYFZOJkBsN6XrShzxpmQRsaaJ9B_iS2W5WbykVjyK8JyavqGy6AF40Vq2B9wIaTf95ystkxokJpP/pub?embedded=true"></iframe>






## GNFA      
- In GNFA the edge has alphabet symbol, ε, and regular expression        
- GNFAs always have a special form that meets the following condition      
    * The start state has transition arrows going to every other state but no arrows coming in from any other state      
    * There is only a single accept state, and it has arrows coming in from every other state but no arrows going to any other state. Furthermore, the accept state is not the same as the start state        
    * Except for the start and accept states, one arrow goes from every state to every other state and also from each state to itself        











## DFA to Regular expression      

The stages in converting a DFA to a regular expression      
    
- DFA → GNFA → Regular expression        

<iframe 
height="842"
width="100%"        
src="https://docs.google.com/document/d/e/2PACX-1vSvjLRES_Kk1Q6cTp2gOtFAbY-8v5zqh6sWlxN4xhTQQAUNahaXUmaakA8rVx-QZ5VxsvJEathKTpdz/pub?embedded=true"></iframe>      
























