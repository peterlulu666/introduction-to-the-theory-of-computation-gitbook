# Finite Automata        

## finite automaton        

1. automatic door controller        
2. elevator controller        
3. dishwashers        
4. electronic thermostat        
5. digital watches        
6. calculators      

## Formal Definition of a Finite Automaton        

- we need the formal definition for two reasons        
    + A formal definition is precise. It resolves any uncertainties about what is allowed in a finite automaton. 
        * number of accept states      
        * number of transitions exiting from a state        
    + A formal definition provides notation
        * Good notation helps you think and express your thoughts clearly        


- A finite automaton has five objects        
    + a set of states        
    + rules for going from one state to another, depending on the input symbol        
    + an input alphabet that indicates the allowed input symbols        
    + a start state        
    + a set of accept states        


- Formal definition      
     + A finite automaton is a 5-tuple (Q, Σ, δ, q0, F)
         * Q is a finite set called the states      
         * Σ is a finite set called the alphabet      
         * δ : Q × Σ− →Q is the transition function      
         * q0∈ Q is the start state      
         * F ⊆ Q is the set of accept states      
    + Note        
        * Since the set F can be empty set `∅` a finite automaton may have zero accept states 
        * Since transitions are described by a function, the function `δ` specifies exactly one next state for each possible combination of state and input symbol  
    + Formalizing      
        * M1 = (Q, Σ, δ, q0, F)      
            - Q = {q1, q2, q3}
            - Σ = {0,1}      
            - δ(q1,0) = q1, δ(q1,1) = q2, δ(q2,0) = q3, δ(q2,1) = q2, δ(q3,0) = q2, δ(q3,1) = q2       
            - δ is described as 
            
                <img src="assets/transition1.svg" alt="transition1" style="zoom:100%;" /> 
                  
            - q1 is the start state      
            - F = {q2}        

- Language of a machine        
    + If A is the set of all strings that machine M accepts, we say that A is the language of machine M and write L(M) = A.        

- Consequences      
    + A machine may accept several strings, but it always recognizes only one language.        
    + If the machine accepts no strings, it still recognizes one language, namely the empty language `∅`.      
    + Language recognized by machine M1 is: A = {w| w contains at least one 1 and an even number of 0s follow the last 1}
    + Conclusion: L(M1) = A, or equivalently, M1 recognizes A.


<img src="assets/m1.svg" alt="m1" style="zoom:100%;" />         

- Machine M2
  + Formally, M2 = ({q1, q2}, {0,1}, δ, q1, {q2}) where δ(q1,0) = q1, δ(q1,1) = q2, δ(q2,0) = q1, δ(q2,1) = q2
  + δ is described as 
  
    <img src="assets/transition2.svg" alt="transition2" style="zoom:100%;" />        

  + state diagram of M2and the formal description of M2 contain the same information, only in different forms
  + A good way to begin understanding any machine is to try it on some sample input strings      
  + language of M2 is L(M2) = {w| w ends in a 1}        

<img src="assets/m2.svg" alt="m2" style="zoom:100%;" />        






<img src="assets/FormalDefinitionOfAFiniteAutomaton1.jpg" alt="FormalDefinitionOfAFiniteAutomaton1" style="zoom:100%;" /> 
<img src="assets/FormalDefinitionOfAFiniteAutomaton2.jpg" alt="FormalDefinitionOfAFiniteAutomaton2" style="zoom:100%;" /> 
<img src="assets/FormalDefinitionOfAFiniteAutomaton3.jpg" alt="FormalDefinitionOfAFiniteAutomaton3" style="zoom:100%;" /> 
 

## Formal definition of computation




<iframe 
height="842"
width="100%"
src="https://docs.google.com/document/d/e/2PACX-1vTyhUM-rJMjU4n5xpA-bso10ywDZLaUZeleOPdKmhxOLnwDSGZHo5qrqrOXqA_TncAFaEskig8Hso_1/pub?embedded=true"></iframe>









## Design finite automaton      



<iframe 
height="842"
width="100%" 
src="https://docs.google.com/document/d/e/2PACX-1vRlYwjeQvCsqvzXujeIMU9FV-x8NAmXyMOkodv92Wg1yu330eAHzn57xXxGwrDIFGgkpIBWtSGU-LAk/pub?embedded=true"></iframe>




## The Regular Operations      


<iframe 
height="842"
width="100%"
src="https://docs.google.com/document/d/e/2PACX-1vQt3_wJbzSP_aji4hYVPY8Eq6bj2tCUBK1FJYJ56ymxAqBV0kKOR63_w3IGAc-TzBUT-jENQDzLD2J2/pub?embedded=true"></iframe>

     

## Practice
<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/12JIZyM_K33bPqefptobDtw2LBZfBj3Mv/preview" width="640" height="480"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1h2d6TMQNGDA6U5qLSO1qNgE-PdGwnYBa/preview" width="640" height="480"></iframe>








