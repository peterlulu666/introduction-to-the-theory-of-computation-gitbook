# NFA to DFA      

<iframe 
height="842"
width="100%" 
src="https://docs.google.com/document/d/e/2PACX-1vSlXsJwYGeL65atieYNpVvqJ2gcpSXgEQqSKgn_F4fD4DYKftMWJTaqc8eE5OxXNj4ZWZ7gWicggp2p/pub?embedded=true"></iframe>




## Question 4

NFA state diagram

<img src="assets/NFA to DFA Question 4 NFA.svg" alt="NFA to DFA Question 4 NFA" style="zoom:100%;" />


NFA transition table

<img src="assets/NFA to DFA Question 4 NFA Table.svg" alt="NFA to DFA Question 4 NFA Table" style="zoom:100%;" /> 

DFA transition table

<img src="assets/NFA to DFA Question 4 DFA Table.svg" alt="NFA to DFA Question 4 DFA Tabl" style="zoom:100%;" /> 



DFA state diagram      
<img src="assets/NFA to DFA Question 4 DFA.svg" alt="NFA to DFA Question 4 DFA" style="zoom:100%;" /> 
     







