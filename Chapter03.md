# Definitions, Theorems, and Proofs      

## Concepts

1. Definitions 
    - describe the objects and notions that we use
2. Mathematical statement
    - a statement expresses that some object has a certain property        
3. proof 
    - a convincing logical argument that a statement is true
4. theorem      
    - a mathematical statement proved true






## Finding proofs        

1. forward direction and reverse direction        
    - To prove a statement of this form, you must prove each of the two directions        
    - one of these directions is easier to prove than the other
2. counterexample        
    - try to find an object that fails to have the property







## Proof by construction

One way to prove such a theorem is by demonstrating how to construct the object.      





## Proof by contradiction        

In one common form of argument for proving a theorem, we assume that the theorem is false and then show that this assumption leads to an obviously false consequence, called a contradiction.      








## Proof by induction        

Proof by induction is an advanced method used to show that all elements of an infinite set have a specified property.        




- Proof by induction      
    + basis        
    + induction step        

    
## Practice
<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1RdcLOQB5mh0xQBz6-_JxCF5sVa6pDcdH/preview" width="640" height="480"></iframe>














